import React from 'react';
import ReactDOM from 'react-dom';
import './global-css/css-variables.scss';
import './global-css/index.scss';
import './global-css/button.scss';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
