import React from "react";
import PropTypes from "prop-types";
import styles from "./spinner.module.scss";

/**
 * Shows a basic spinner, if shouldShowSpinner is true it will render
 * @param {Boolean} props
 */
const Spinner = props => {
  // If the spinner should not show we return null so the component is not rendered
  return !props.shouldShowSpinner ? null : (
    <div className={styles.container}>
      <div className={styles.spinner} />
    </div>
  );
};

// Static type checking the props match what is expected
Spinner.propTypes = {
  shouldShowSpinner: PropTypes.bool
};

// Uses React.memo() to ensure the component only renders when the meals change.
// See https://reactjs.org/docs/react-api.html#reactmemo for more info.
export default React.memo(Spinner);
