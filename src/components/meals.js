import React from "react";
import PropTypes from "prop-types";

/**
 * Builds an image src URL based on the API documentation - https://spoonacular.com/food-api/docs/show-images
 * @param {string} baseUrl
 * @param {string} mealId
 * @returns {string} - Image URL
 */
const getImageUrl = (baseUrl, mealId) => {
  return `${baseUrl}${mealId}-636x393`; // Build the URL
};

/**
 * Returns an array excluding duplicate meals.
 * @param {Array} meals
 * @returns {Array}
 */
const preventDuplicateMeals = meals => {
  const mealsSet = new Set(meals); // Create as a Set as Sets automatically remove any duplicates https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
  return [...mealsSet]; // We need to convert the set back to an array as we need to use Array.map and Array.length
};

/**
 * Loops through each meal and render it.
 * @param {Array} props
 * @returns {any[]}
 */
const Meals = props => {
  const meals = preventDuplicateMeals(props.meals);
  console.log(meals);

  // Return null if meals is not populated to prevent the component from rendering
  return !meals.length
    ? null
    : meals.map(meal => {
        return (
          <div key={meal.id}>
            <h2>{meal.title}</h2>
            <p>Ready in {meal.readyInMinutes} minutes</p>
            <p>Has {meal.servings} servings</p>
            <img
              src={getImageUrl(props.imageBaseUrl, meal.id)}
              alt={meal.title}
            />
            <hr />
          </div>
        );
      });
};

// Static type checking the props match what is expected
Meals.propTypes = {
  imageBaseUrl: PropTypes.string.isRequired,
  meals: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      image: PropTypes.string,
      imageUrls: PropTypes.arrayOf(PropTypes.string),
      title: PropTypes.string.isRequired,
      readyInMinutes: PropTypes.number.isRequired,
      servings: PropTypes.number.isRequired
    })
  )
};

// Uses React.memo() to ensure the component only renders when the meals change.
// See https://reactjs.org/docs/react-api.html#reactmemo for more info.
export default React.memo(Meals);
