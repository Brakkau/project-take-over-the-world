import React from "react";
import PropTypes from "prop-types";
import styles from "./message.module.scss";

/**
 * Renders a message, can apply types for styling such as error message or info message
 * @param {Object} props
 */
const Message = props => {
  const message = props.message;

  // If the message has content it will return it, if not it will return null which makes React render nothing
  return !message.content ? null : (
    <div className={`${styles[message.type]} ${styles.message}`}>
      {message.content}
    </div>
  );
};

// Static type checking the props match what is expected
Message.propTypes = {
  message: PropTypes.shape({
    content: PropTypes.string,
    type: PropTypes.string
  })
};

// Uses React.memo() to ensure the component only renders when the meals change.
// See https://reactjs.org/docs/react-api.html#reactmemo for more info.
export default React.memo(Message);
