import React from "react";
import PropTypes from "prop-types";

/**
 * Either return the totals if populated or null if empty.
 * We return null to prevent the component from being rendered, for more info see
 * see https://reactjs.org/docs/conditional-rendering.html#preventing-component-from-rendering
 * @param {Object} props
 */
const Totals = props => {
  const totals = props.totals; // Cache totals for performance
  const isTotalsPopulated = Object.keys(totals).length; // Convert object to array to get the length (objects do not have a length property)

  // If isTotalsPopulated return a falsey value (i.e 0) return null else render the JSX
  return !isTotalsPopulated ? null : (
    <div>
      <p>Calories: {totals.calories}</p>
      <p>Protein: {totals.protein}</p>
      <p>Fat: {totals.fat}</p>
      <p>Carbohydrates: {totals.carbohydrates}</p>
      <hr />
    </div>
  );
};

// Static type checking the props match what is expected
Totals.propTypes = {
  totals: PropTypes.objectOf(PropTypes.number).isRequired
};

// Uses React.memo() to ensure the component only renders when the meals change.
// See https://reactjs.org/docs/react-api.html#reactmemo for more info.
export default React.memo(Totals);
