import React, { Component } from "react";
import PropTypes from "prop-types";
import formStyles from "./form.module.scss";

class mealInput extends Component {
  constructor(props) {
    super(props);

    // Set the default state.
    this.state = {
      calories: 0,
      dayQuantity: 0
    };
  }

  // @toDo: Add handleInput function and update both calories and dayQuantity
  // @toDo: disable/enable button submit when both inputs are populated (based on state)
  updateDaysQuantity(event) {
    const value = event.target.value; // Take the value from the input

    // Update the state
    this.setState({
      dayQuantity: value
    });
  }

  validateInputs() {
    const state = this.state;
    console.log(!!state.calories && !!state.dayQuantity);
  }

  render() {
    const state = this.state; // Cache the state for performance

    return (
      <>
        <h1>Meal = Calories</h1>

        <form
          className={formStyles.form}
          onSubmit={event => this.props.handleFormSubmit(event)}
        >
          <div className={formStyles.group}>
            <input name="calories" type="number" required="required" />
            <span className={formStyles.highlight}></span>
            <span className={formStyles.bar}></span>
            <label htmlFor="calories">Calories</label>
          </div>
          <div className={formStyles.group}>
            <input
              onChange={event => this.updateDaysQuantity(event)}
              className={formStyles.input}
              name="timeFrame"
              type="number"
              required="required"
            />
            <span className={formStyles.highlight}></span>
            <span className={formStyles.bar}></span>
            <label className={formStyles.label} htmlFor="timeFrame">
              Day{state.dayQuantity > 1 ? "s" : ""}
            </label>
          </div>
          <h2>Extra's</h2>
          <div className={formStyles.group}>
            <input className={formStyles.inputExtras} name="diet" type="text" />
            <span className={formStyles.highlight}></span>
            <span className={formStyles.bar}></span>
            <label className={formStyles.label} htmlFor="diet">
              Diet
            </label>
          </div>
          <div className={formStyles.group}>
            <input
              className={formStyles.inputExtras}
              name="exclude"
              type="text"
            />
            <span className={formStyles.highlight}></span>
            <span className={formStyles.bar}></span>
            <label className={formStyles.label} htmlFor="exclude">
              exclude
            </label>
          </div>
          <div className="btn-box">
            <button className="btn btn-submit" type="submit">
              submit
            </button>
          </div>
        </form>
      </>
    );
  }
}

// Static type checking the props match what is expected
mealInput.propTypes = {
  handleFormSubmit: PropTypes.func
};

// Uses React.memo() to ensure the component only renders when the meals change.
// See https://reactjs.org/docs/react-api.html#reactmemo for more info.
export default React.memo(mealInput);
