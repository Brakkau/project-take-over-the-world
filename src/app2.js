import React, { Component } from "react";

import MealConfig from "./components/meal-input/meal-input";
import Meal from "./components/meals";
import Totals from "./components/totals";
import Spinner from "./components/spinner/spinner";
import Message from "./components/message/message";

import Typography from "typography";
import fairyGatesTheme from "typography-theme-fairy-gates";
const typography = new Typography(fairyGatesTheme);
typography.injectStyles();

    const data = {
        meals: [],
        nutrients: {},
        id: {}
    };

    const message = {
        content: "",
        type: ""
    };

    const posts = [];
    const isLoading = false;
    const error = null;
    const calories = "";
    const timeFrame = "";

    const urls = {
        images: "https://spoonacular.com/recipeImages/"
    }

  /**
   * Builds the request URL, kept in a separate function to keep the actual request simple
   * @returns {string} - The request URL and parameters
   */
  const buildRequestUrl = () => {
    const state = this.state; // Store the state in a local variable so it's cached
    const baseUrl =
      "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/mealplans/generate?";
    const calories = `targetCalories=${state.calories}`;
    const timeFrame = `&timeFrame=${state.timeFrame}`;
    const diet = `&diet=${state.diet}`;
    const exclude = `&exclude=${state.exclude}`;
    return baseUrl + calories + timeFrame + diet + exclude; // Build the URL
  }

  const buildInstructionsRequestUrl = () => {
    this.state.data.meals.map(data=>{
        return this.data.id = data.meals.id
    })

    const instructionID = this.mealId;
    const instructionsUrl = `https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/${instructionID}/analyzedInstructions`;
    return instructionsUrl;
  }

  /**
   * Headers for the request, kept in a seprate method to keep the request easy to read.
   * @returns {{headers: {"X-RapidAPI-Key": string}}}
   */
  const requestHeaders = () => {
    return {
      headers: {
        "X-RapidAPI-Key": "0330d376ebmshfe385f08949f61bp162db6jsn87c670e5d1a6"
      }
    };
  }

  /**
   * Uses fetch to send an request to retrieve the meals and nutritional information.
   */
  const getMeals = () => {
      isLoading = true
    };

    fetch(this.buildRequestUrl(), this.requestHeaders())
      .then(response => response.json())
      .then(data => { 
          data = data, // Update state.data with the data from the response
          isLoading = false // Set state.isLoading to false
      })
    .catch(error => {
        // If we catch an error we should report it to the user (by updating the state)
        message.content = error.message,
        message.type- "error"
        isLoading= false
      });

  /**
   * Updates the state and runs the method to fire the AJAX request.
   * @param {Event} event - Form submit event
   */
  const handleMealInfoFormSubmit = event => {
    event.preventDefault();
    const calories = event.target.calories.value; // Grab the calories from the form's inputs
    const timeFrame = event.target.timeFrame.value; // Grab the time frame from the form's inputs
    const diet = event.target.diet.value; // Grab the diet from the form's inputs
    const exclude = event.target.exclude.value; // Grab the exclude from the form's input

    // We need to run getMeals in the setState callback to ensure the state is correctly set when building the request URL
    // Else the data may end up 1 behind what the user inputs.
    this.setState(
      {
        calories,
        timeFrame,
        diet,
        exclude
      },
      this.getMeals,
    );
  }


  render() {
    const state = this.state; // Cache the state for performance

    return (
      <div className="container">
        <Spinner shouldShowSpinner={state.isLoading} />
        <Message message={state.message} />
        <MealConfig
          handleFormSubmit={event => this.handleMealInfoFormSubmit(event)}
        />
        <Meal meals={state.data.meals} imageBaseUrl={this.urls.images} />
        <Totals totals={state.data.nutrients} />
      </div> 
    );
  }
}
